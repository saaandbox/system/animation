# Prisma Animations

A test site for Prisma animations library. 


# Usage

**JS**

Place the minified lottie-player `<script>` inside your `<head>` section. This is ONLY applicable if you want to use the JSON file approach.
> `<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>`

**JSON**

This is the recommended approach to use the animation. Insert the JSON file path inside the `<lottie-player>` tags.
> `<lottie-player src="./json/coming-soon.json" style="width:180px; height:180px;" speed="1" loop autoplay></lottie-player>`

**GIF**

Another approach to use the animation is with a GIF format. Simply insert the GIF file path inside the `<img>` tags.
> `<img src="./gif/page-404.gif" width="180" height="180" alt="" title="page-404">`



